﻿using UnityEngine;
using System.Collections;

public class LookAtCamera : MonoBehaviour {

    void Update(){
        if(Camera.main!=null)
        this.transform.forward =Camera.main.transform.forward;
    }
}
