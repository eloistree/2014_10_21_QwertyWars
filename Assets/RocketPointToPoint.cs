﻿using UnityEngine;
using System.Collections;
using System;

public class RocketPointToPoint : MonoBehaviour {


    public bool rocketThrow = false;
    
    public Transform to;
    public float speed = 10f;
    public float increaseBySecond = 100f;
    public float maxSpeed = 500f;
    public float lifeTime=5f;
    public float time;
    public float explosionDistance=0.2f;

    public GameObject explosionPref;
    public void Awake() 
    {
        gameObject.SetActive(false);
    }
	void Update () {
        if (!rocketThrow) return;
        if (speed < maxSpeed) 
        speed += increaseBySecond * Time.deltaTime;
        time += Time.deltaTime;

        if(to!=null)
        transform.LookAt(to);
        Vector3 origine = transform.position;
        transform.Translate(Vector3.forward * speed * Time.deltaTime);
        try { 
            if (Vector3.Distance(origine, transform.position) > Vector3.Distance(origine, to.position)-explosionDistance) Explode();
            if (time > lifeTime) Explode();
            else if (Vector3.Distance(transform.position, to.position) < explosionDistance)
                Explode();
           
        }catch(Exception e)
        {
                Explode();
            }
	
	}
    public void Throw() 
    {
        rocketThrow = true;
        if (to != null)
            transform.LookAt(to);
        gameObject.SetActive(true);
                
    }

    private void Explode()
    {
        if(explosionPref!=null)
        GameObject.Instantiate(explosionPref, transform.position, transform.rotation);
        Destroy(this.gameObject);
    }
}
