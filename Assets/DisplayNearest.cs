﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DisplayNearest : MonoBehaviour {


    public Text displayTarget_Word;
    private int count = 0;
    public Text displayTarget_Count;
    public Transform nearestFrom;
    public float refresh = 0.5f;
    private float countDown;

    void Awake()
    {
        QwertyTarget.onTargetIterated += Refresh;
        QwertyTarget.onTargetEnded += Refresh;
        QwertyTarget.onTargetEnded += AddOne;
        QwertyTarget.onTargetCreated += Refresh;
    }

    private void AddOne(QwertyTarget obj, string word)
    {
        count++;
        displayTarget_Count.text = "" + count;
    }

    private void Refresh(QwertyTarget target)
    {
        Refresh();
    }

    private void Refresh(QwertyTarget obj, string word)
    {
        Refresh();
    }

    private void Refresh(QwertyTarget obj, string word, string newWord)
    {
        Refresh();
    }
	void Update () {
        if (displayTarget_Word == null || nearestFrom == null) return;

        countDown -= Time.deltaTime;
        if (countDown <= 0) 
        {
            countDown = refresh;
            Refresh();

        }
	}

    private void Refresh()
    { QwertyTarget target =null;
        if(nearestFrom!=null)
         target = QwertyTarget.GetNearestTarget(nearestFrom.position);

        displayTarget_Word.text = target == null ? "No target" : target.GetWordLeft();
    }
}
