﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class QwertyTarget : MonoBehaviour {

    public static List<QwertyTarget> targets = new List<QwertyTarget>();
    public delegate void TargetCreated(QwertyTarget target);
    public static TargetCreated onTargetCreated;
    public delegate void TargetIterated(QwertyTarget obj, string word, string newWord);
    public static TargetIterated onTargetIterated;
    public delegate void TargetEnded(QwertyTarget obj, string word);
    public static TargetEnded onTargetEnded;

    public static QwertyTarget GetNearestTarget( Vector3 from)
    {
        IEnumerable<QwertyTarget> correspondingTarget = from t in targets where ! t.isDeath select t;
        return GetNearestTarget(correspondingTarget, from);
    }
    public static QwertyTarget GetNearestTarget(IEnumerable<QwertyTarget> targetsSelected, Vector3 from) 
    {
        float distance =100000000f;
        QwertyTarget targNearest=null;
        foreach (QwertyTarget targ in targetsSelected) 
        {
            if (targ == null) continue;
            float d =Vector3.Distance(targ.transform.position, from); 
            if (d < distance)
            {
                distance = d;
                targNearest = targ;
            }
        }
        return targNearest;
    }
    public static QwertyTarget GetNearestTarget(Vector3 from, char startingWith)
    {
        IEnumerable<QwertyTarget> correspondingTarget = from t in targets where ! t.isDeath&& t.GetNextLetter() == startingWith select t;
        return GetNearestTarget(correspondingTarget, from);
    }



    public delegate void CursorChanged(QwertyTarget obj,string word, string newWord);
    public CursorChanged onCursorChange;
    public delegate void Death(QwertyTarget obj,string word);
    public Death onDeath;
    public string word="EloiStree";
    public int cursor;
    public bool isDeath;

    public void Start() 
    {
        targets.Add(this);
        if(onTargetCreated!=null)
        onTargetCreated(this);
        word = MostUsedWord.GetRandomWordFromText();
        //onCursorChange = (o, w, n) => { print("I lose life : " + w+" to "+n); };
        //onDeath = (o, w) => {print("I am death : "+w); };

        //while (Next());
    
    }

    public char GetNextLetter()
    {

        return cursor>=word.Length?' ':word[cursor];
    }
    public bool Next(char letter) 
    {
        return GetNextLetter()==letter ? Next() : false;
    }
    public bool Next()
    {
        if (isDeath) return false;

        cursor++;
        if (cursor >= word.Length) 
        {
            isDeath = true;
            cursor = word.Length ;
        }
        
            if (onCursorChange != null)
                onCursorChange(this, word, GetWordLeft());
            if (onTargetIterated != null)
            onTargetIterated(this, word, GetWordLeft());
       
        if (isDeath)
        {
            if (onDeath != null)
                onDeath(this, word);
            if (onTargetEnded != null)
                onTargetEnded(this, word);
        }
        return !isDeath;
    }

    public string GetWordLeft()
    {
        return word.Substring(cursor);
    }


}
