
var explosionPrefab : GameObject;

function OnCollisionEnter(collision : Collision) {

    //var contact = collision.contacts[2];
    var tag = collision.gameObject.tag;
    var rot = transform.rotation;
    var pos = transform.position;
   var explosion =  GameObject.Instantiate(explosionPrefab, pos, rot);
    
    Destroy (gameObject, 0);
    if(explosion!=null)
    Destroy (explosion, 5);

}