﻿using UnityEngine;
using System.Collections;

public class ProjectilBuilder : MonoBehaviour {

    public GameObject rocketPref;


    public GameObject FireProjectil(Transform from,Transform target) 
    {
        if (rocketPref == null || target == null) { Debug.LogWarning("No rocket or no target",this); return null; }
       GameObject gamo = GameObject.Instantiate(rocketPref, from.position, from.rotation) as GameObject;
       if (gamo == null) return null;
       RocketPointToPoint rocketPTP = gamo.GetComponent<RocketPointToPoint>() as RocketPointToPoint;
       if (rocketPTP != null)
       {
           rocketPTP.to = target;
           rocketPTP.Throw();
       }
        EffectSettings effectSetting = gamo.GetComponent<EffectSettings>() as EffectSettings;
        if (effectSetting != null) 
        {
            effectSetting.Target = target.gameObject;
        }
        return gamo;
    }
}
