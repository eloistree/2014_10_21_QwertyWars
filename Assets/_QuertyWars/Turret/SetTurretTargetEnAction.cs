﻿using UnityEngine;
using System.Collections;

public class SetTurretTargetEnAction : MonoBehaviour {

    public KeyCode focus=KeyCode.F;
    public KeyCode standBy = KeyCode.Escape;
    public Turret turret;
    public Transform target;

	void Update () {
        if(turret==null||target==null)return;
        if (Input.GetKeyDown(focus))
        {
            turret.LookAt(target.position);
        }
        if (Input.GetKeyDown(standBy))
        {
            turret.StandBy();
        }
	}
}
