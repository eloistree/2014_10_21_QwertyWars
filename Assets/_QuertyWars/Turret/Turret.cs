﻿using UnityEngine;
using System.Collections;

public class Turret : MonoBehaviour,ITurret {

    public Transform direction;
    public bool withRotation=true;
    Quaternion lastRotation;
    Quaternion lookAtRotation;
    public float rotationMaxSpeed=20f;
    public float timeToFocus=0.5f;
    private float timeLeftToFocus=0f;

    public bool withStandBy=true;
    public float timeBeforeStandBy=3f;
    private float timeLastAction;

    private Transform target;

    public ProjectilBuilder rocketBuilder;
    public ProjectilBuilder laserBuilder;
    public Transform [] popPointRocket;
    public Transform [] popPointLaser;
    public Transform GetRocketPopPoint()
    {
        if (popPointRocket == null || popPointRocket.Length == 0) return null;
        return popPointRocket[Random.Range(0, popPointRocket.Length)];

    }
    public Transform GetLaserPopPoint()
    {
        if (popPointLaser == null || popPointLaser.Length == 0) return null;
        return popPointLaser[Random.Range(0, popPointLaser.Length)];

    }

    public delegate void RocketFired(Transform rocket, Transform target);
    public RocketFired onRocketFired;
    public delegate void RocketLaser(Transform laser, Transform target);
    public RocketLaser onLaserFired;
    public delegate void FocusReceived(Vector3 point);
    public FocusReceived onFocusReceived;
    public delegate void FocusEnd(Vector3 point);
    public FocusEnd onFocusFinish;  
        
    void Update() 
    {
        if (onFocusFinish!=null && timeLeftToFocus>0f &&  timeLeftToFocus - Time.deltaTime<=0f) {
            onFocusFinish(GetTargetPoint());
        }

        timeLeftToFocus -= Time.deltaTime;
        if (timeLeftToFocus > 0f)
        {
            if(withRotation)
            transform.rotation = Quaternion.Lerp(transform.rotation, lookAtRotation, 1f - (timeLeftToFocus / timeToFocus));
        }
        else { 

            timeLeftToFocus = 0f; 
        }

        if (timeLastAction < timeBeforeStandBy && timeLastAction + Time.deltaTime > timeBeforeStandBy) 
        {
            StandBy();
        }
        timeLastAction += Time.deltaTime;
       
    }

    public void FireRocket(Transform target, bool withLookAt)
    {
        LookAt(target);
        if (withLookAt) Invoke("FireRocket", timeToFocus);
        else FireRocket();
    
    }
    public void FireRocket()
    {

        if (popPointRocket == null || rocketBuilder == null) return;
        GameObject gamo = rocketBuilder.FireProjectil(GetRocketPopPoint(), target);
        if (onRocketFired != null && gamo != null) onRocketFired(gamo.transform, target);
        timeLastAction = 0f;
    }
    public void FireLaser(Transform target, bool withLookAt)
    {
        this.target = target;
        LookAt(target);
        if (withLookAt) Invoke("FireLaser", timeToFocus);
        else FireLaser();

    }
    public void FireLaser()
    {

        if (popPointLaser == null || laserBuilder == null) return;
        GameObject gamo = laserBuilder.FireProjectil(GetLaserPopPoint(), target);
        if (onLaserFired != null && gamo != null) onLaserFired(gamo.transform, target);
        timeLastAction = 0f;
    }

    private Vector3 GetTargetPoint()
    {
        return target == null ? Vector3.zero : target.position;
    }

    public void LookAt(Vector3 position) { LookAt(position,false); }
    public void LookAt(Vector3 position,bool isNotAction)
    {
        lookAtRotation = Quaternion.LookRotation(position - transform.position);
        timeLeftToFocus = timeToFocus;
        if(isNotAction)
        timeLastAction = 0f;
        if(onFocusReceived!=null)
        onFocusReceived(GetTargetPoint());
    }
    public void LookAt(Transform target)
    {
        if (target == null) return;
        LookAt(target.position,false);
    }



    public void StandBy()
    {
        if (direction != null)
            LookAt(direction.position + direction.forward * 100f,true);
        
    }





    public void Fire(Transform target)
    {
        this.target = target;
        FireRocket();
    }
}
