﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;
using System.Collections.Generic;

public class MostUsedWord : MonoBehaviour {

	// Use this for initialization
    public string text="";
    public static List<string> words=new List<string>();
	void Start () {

        Regex rgx = new Regex("[^a-zA-Z\\s]");
        string result = rgx.Replace(text, " ");
        text = result;
        string [] wordsTab = text.Split(' ');
        foreach (string s in wordsTab)
        {
            if (s == null) continue;
            string tmp = s.Trim();
            if (tmp.Length > 0 && !tmp.Equals(" ") && !tmp.Equals(""))
            {
                //print("> " + s);
                words.Add(tmp);
            }
        }
	}

    public static string GetRandomWordFromText()
    {
        if (words == null || words.Count <= 0) return "NoWords";
        return words[Random.Range(0, words.Count)];
        
    }
}
