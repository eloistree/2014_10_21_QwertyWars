﻿using UnityEngine;
using System.Collections;

public interface ITurret  {

    void LookAt(Vector3 position);
    void StandBy();
    void Fire(Transform target);

}
