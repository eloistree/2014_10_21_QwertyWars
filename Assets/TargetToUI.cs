﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TargetToUI : MonoBehaviour {
    public QwertyTarget target;
    public Text name;
	// Use this for initialization
	void Start () {
        if (target == null || name == null) { Destroy(this.gameObject); return; }
        name.text = target.word;
        target.onCursorChange += (t, o, n) => { name.text = n; };
        target.onDeath += (t, n) => { name.text = ""; };
	
	}

    public void OnCollisionEnter(Collision col) 
    {
        if (!col.gameObject.tag.Equals("Explosion")) return;
        Explosition();
        
    
    }

    private void Explosition()
    {
        Destroy(this.gameObject, 0.5f);
    }
	
}
