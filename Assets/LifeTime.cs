﻿using UnityEngine;
using System.Collections;

public class LifeTime : MonoBehaviour {

    public float time=10f;
	void Start () {
        Destroy(this.gameObject, time);
        Destroy(this);
	}
	
}
