﻿using UnityEngine;
using System.Collections;

public class InitCanvasTarget : MonoBehaviour {

    public Canvas canvas;
    void Start () {

        if (canvas != null) canvas.worldCamera = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
