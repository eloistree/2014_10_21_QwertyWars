﻿using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class ShipBuilder : MonoBehaviour {

    public GameObject protailPrefab;
    public GameObject shipPrefab;


    public Transform player;
    public Transform spaceShit;
    public Transform spaceShipDroppingZone;
    public List<Transform> droppingZone;
    public float apparitionRange = 100f;
    public float height = 10f;


    void Start() { StartCoroutine(RandomDropping()); }

    public IEnumerator RandomDropping(float timeFrom=4f, float timeTo=10f,int nombreMin=2,int nombreMax=4, float between=0.5f, float variation=0.2f) 
    {
        do{
            float nextNombre = Random.Range(nombreMin,nombreMax);
            for (int i = 0; i < nextNombre; i++) 
            {
                ThrowShip(shipPrefab,protailPrefab);
                yield return new WaitForSeconds(between+Random.Range(-variation,variation));
        
            }

            float nextTime = Random.Range(timeFrom, timeTo);
            yield return new WaitForSeconds(nextTime);
    
        } while(player!=null || spaceShit!=null);
    }

    private void ThrowShip(GameObject shipPrefab, GameObject protailPrefab)
    {
        Transform droppingZone =  GetDroppingZone();
        if(droppingZone==null)return;
        Vector3 whereDropping = droppingZone.position;
        whereDropping.x += Random.Range(-apparitionRange, apparitionRange);
        whereDropping.z += Random.Range(-apparitionRange, apparitionRange);
        whereDropping.y += Random.Range(-height, height);

        if (protailPrefab != null)
        {
            GameObject.Instantiate(protailPrefab, whereDropping, droppingZone.rotation);
        }

        if (shipPrefab != null)
        {
            GameObject.Instantiate(shipPrefab, whereDropping, droppingZone.rotation);
        }
    }


    public Transform GetDroppingZone() 
    {
        if (spaceShit == null || player == null) return null;
        Transform nearestValide = null ;
        float nearestOfPLayer=1000000f;
        foreach (Transform dz in droppingZone)
        {
            float dShipZone = Vector3.Distance(dz.position, spaceShit.position);
            float dPlayerZone = Vector3.Distance(dz.position, player.position);

            if (dShipZone < dPlayerZone) 
            {
                if (dPlayerZone < nearestOfPLayer)
                {
                    nearestOfPLayer = dPlayerZone;
                    nearestValide = dz;
                }

            
            }
        
        }

        if (nearestValide == null) return spaceShipDroppingZone;
       
        return nearestValide;
    }

}
