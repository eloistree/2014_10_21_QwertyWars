﻿using UnityEngine;
using System.Collections;

public class SpaceShipLife : MonoBehaviour {

    public float life = 1;
    public float startLife = 50;
    public GameObject shield;
    public GameObject ship;
    public GameObject shieldReactionPrefab;

    public delegate void SpaceShipDestroy(GameObject ship);
    public SpaceShipDestroy onDestroy;
    public delegate void SpaceShipHit(GameObject ship,float newLife,Vector3 where,Transform what);
    public SpaceShipHit onHit;
    void Start() 
    {
        if(ship==null){Destroy(this);return;}
        life = startLife;
        onDestroy += DestroyShip;
        onHit += ShieldReaction;
    }


    private void ShieldReaction(GameObject ship, float newLife, Vector3 where, Transform what)
    {
        
        if (shieldReactionPrefab == null) return;
        if (newLife < 5) return;
        Vector3 direction = where - ship.transform.position;
        GameObject.Instantiate(shieldReactionPrefab, where, Quaternion.LookRotation(direction));
       


    }
    private void DestroyShip(GameObject ship)
    {
        if (ship != null) { Destroy(ship, 10f); }
    }
    public void Hit(Vector3 where, Transform what)
    {
        
        if (life <= 0) return;
        life--;
        onHit(ship, life,where, what);
        if (life <= 0) 
        {
            onDestroy(ship);
        }


    }


    public void OnCollisionEnter(Collision col)
    {

        if (!(col.gameObject.tag.Equals("Explosion") || col.gameObject.tag.Equals("Rocket"))) return;

       
        Vector3 where =col.contacts[0].point; 
        Hit(where, col.gameObject.transform);


        if (shield != null)
        {
            Quaternion quadDir = Quaternion.LookRotation(where - transform.position);
            GameObject gamo = GameObject.Instantiate(shield, transform.position, quadDir) as GameObject;
            gamo.transform.parent = transform;
            gamo.transform.localScale = Vector3.one;
        }
    }


}
