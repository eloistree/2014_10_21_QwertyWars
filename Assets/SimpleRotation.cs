﻿using UnityEngine;
using System.Collections;

public class SimpleRotation : MonoBehaviour {

    public float rotation=10f;

    void Update () {

        transform.Rotate(Vector3.up * (rotation * Time.deltaTime));
	}
}
