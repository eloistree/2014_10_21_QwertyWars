﻿using UnityEngine;
using System.Collections;

public class InputToFire : MonoBehaviour {

    public Turret[] turrets;
    public Transform target;
    public Transform player;
    public KeyCode[] tracked = new KeyCode[] { KeyCode.A ,KeyCode.	B 	,KeyCode.C 	,KeyCode.D 	
,KeyCode.E ,KeyCode.	F 	,KeyCode.G 	,KeyCode.H 	,KeyCode.I 	,KeyCode.J
,KeyCode.K 	,KeyCode.L 	,KeyCode.M 	,KeyCode.N 	,KeyCode.O 	,KeyCode.P
,KeyCode.Q 	,KeyCode.R 	,KeyCode.S 	,KeyCode.T 	,KeyCode.U 	,KeyCode.V
	,KeyCode.W 	,KeyCode.X 	,KeyCode.Y 	,KeyCode.Z 	};


    void Awake()
    {
        player = GameObject.FindWithTag("Player").transform;
        QwertyTarget.onTargetIterated += (t, ow, nw) => { 
            
            foreach(Turret turret in turrets)
            if (turret != null && t != null) turret.FireLaser(t.transform, false);
        };
        QwertyTarget.onTargetEnded += (t, ow) => {

            foreach (Turret turret in turrets)
            if (turret != null && t!=null) turret.FireRocket(t.transform,false);  
        };
    }
	// Update is called once per frame
	void Update () {
        if (turrets == null|| turrets.Length<=0) { Destroy(this); return; }
       
       KeyCode kc =GetKeyDown(); 
        if(kc != KeyCode.None)
        if (Input.GetKeyDown(kc))
        {
            bool maj = Input.GetKey(KeyCode.LeftShift);
            QwertyTarget qt = SelectTargetNearest(GetKeyToChar(kc, maj), ref target, player);
            if (qt != null) 
            {
                qt.Next();
            }
        }
        
      

        
	
	}

    private KeyCode GetKeyDown() 
    {
        foreach (KeyCode kc in tracked) {
            if (Input.GetKeyDown(kc)) return kc;
        }
        return KeyCode.None;
    }
    private QwertyTarget SelectTargetNearest(char c, ref Transform target,Transform from)
    {
        QwertyTarget t = QwertyTarget.GetNearestTarget(from.position, c);
        if (t == null) { Debug.Log("No target starting with: "+c); target = null; }
        else target = t.transform;
        return t;
    }
    private void SelectTargetNearest(Transform from)
    {
        QwertyTarget t = QwertyTarget.GetNearestTarget(from.position);
        if (t == null) { Debug.Log("No target"); target = null; }
        else target = t.transform;
    }

    public char GetKeyToChar(KeyCode key,bool maj=false) 
    {
        switch(key)
        {
          case KeyCode.A: return !maj?'a':'A';
          case KeyCode.B: return !maj?'b':'B';
          case KeyCode.C: return !maj?'c':'C';
          case KeyCode.D: return !maj?'d':'D';
          case KeyCode.E: return !maj?'e':'E';
          case KeyCode.F: return !maj?'f':'F';
          case KeyCode.G: return !maj?'g':'G';
          case KeyCode.H: return !maj?'h':'H';
          case KeyCode.I: return !maj?'i':'I';
          case KeyCode.J: return !maj?'j':'J';
          case KeyCode.K: return !maj?'k':'K';
          case KeyCode.L: return !maj?'l':'L';
          case KeyCode.M: return !maj?'m':'M';
          case KeyCode.N: return !maj?'n':'N';
          case KeyCode.O: return !maj?'o':'O';
          case KeyCode.P: return !maj?'p':'P';
          case KeyCode.Q: return !maj?'q':'Q';
          case KeyCode.R: return !maj?'r':'R';
          case KeyCode.S: return !maj?'s':'S';
          case KeyCode.T: return !maj?'t':'T';
          case KeyCode.U: return !maj?'u':'U';
          case KeyCode.V: return !maj?'v':'V';
          case KeyCode.W: return !maj?'w':'W';
          case KeyCode.X: return !maj?'x':'X';
          case KeyCode.Y: return !maj?'y':'Y';
          case KeyCode.Z: return !maj?'z':'Z';
        }
        return ' ';
    }
}
