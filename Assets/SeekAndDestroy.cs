﻿using UnityEngine;
using System.Collections;

public class SeekAndDestroy : MonoBehaviour {

    public Transform target;
    public Transform missileOut;

    public float fireDistance=100f;
    public ProjectilBuilder rocketBuilder;
    public float fireCountFrom = 4f;
    public float fireCountTo = 8f;
    private float fireCD;
    public float stopMovingDistance = 0f;
    private float stopMovingDistanceVarRandom = 20f;


    public float speed = 2f;
    public float distanceOfTargetForLight = 150f;
    public float lightSpeed = 300f;

    void Awake() {
        stopMovingDistance += Random.Range(-stopMovingDistanceVarRandom, stopMovingDistanceVarRandom);

    }
    void Start () {
       GameObject gamo = GameObject.FindWithTag("Player");
       target = gamo.transform;

	}

    void OnCollisionEnter(Collision col) 
    {
        if (col.gameObject.tag.Equals("Player")) 
        {
        
        Application.LoadLevel(Application.loadedLevel);
        }
        
    }

	void Update () {
        if (target == null) return;
        float targetDistance =Vector3.Distance(transform.position, target.position);
        //if ( targetDistance < stopMovingDistance) return;
        transform.Translate(Vector3.forward * ((targetDistance > distanceOfTargetForLight ? lightSpeed : speed) * Time.deltaTime));
     
        transform.LookAt(target);

        if(rocketBuilder!=null)
        fireCD -= Time.deltaTime;
        if (fireCD < 0) 
        {
            fireCD = Random.Range(fireCountFrom, fireCountTo);
            if (targetDistance < fireDistance)
            {
                rocketBuilder.FireProjectil(missileOut, target);
            
            }
        }

	}

}
